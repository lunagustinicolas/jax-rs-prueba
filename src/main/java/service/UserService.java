package service;

import domain.User;
import repository.UserRepository;

import javax.inject.Inject;
import java.util.List;

public class UserService {

    private UserRepository userRepository;

    public UserService() {
        this.userRepository = new UserRepository();
    }
    public List<User> findAll() {
        return userRepository.getUsers();
    }

    public List<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public List<User> createUser(User user) {
        return userRepository.createUser(user);
    }
}
