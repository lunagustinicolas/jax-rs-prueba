package controller;

import domain.User;
import service.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
public class UserController {

    private static EntityManager manager;
    private static EntityManagerFactory entityManagerFactory;
    private UserService userService;

    public UserController() {
        this.userService = new UserService();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {
        List<User> listaUsuarios =  userService.findAll();
        return Response.ok(listaUsuarios).build();
    }

    @POST
    @Path("/createUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(User userRequest) {
        List<User> listaUsuarios =  userService.createUser(userRequest);
        return Response.ok(listaUsuarios).build();
    }

    @GET
    @Path("/findById/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {
        List<User> listaUsuarios =  userService.findById(id);
        return Response.ok(listaUsuarios).build();
    }

    @GET
    @Path("/404")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getError() {
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
