package test;

import domain.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class EntityManagerUser {

    private static EntityManager manager;
    private static EntityManagerFactory entityManagerFactory;

    public static void main(String args[]) {
        entityManagerFactory = Persistence.createEntityManagerFactory("bicho");
        manager = entityManagerFactory.createEntityManager();
        List<User> listaUsuarios = (List<User>) manager.createQuery("Select user FROM user", User.class).getResultList();
        System.out.print(listaUsuarios);

    }

}
