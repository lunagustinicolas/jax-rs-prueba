package repository;

import domain.User;

import javax.persistence.*;
import javax.ws.rs.core.Response;
import java.util.List;

public class UserRepository {

    private static EntityManager manager;
    private static EntityManagerFactory entityManagerFactory;

    public List<User> getUsers() {
        entityManagerFactory = Persistence.createEntityManagerFactory("bicho");
        manager = entityManagerFactory.createEntityManager();
        List<User> listaUsuarios =  manager.createQuery("SELECT u FROM User u").getResultList();
        return listaUsuarios;
    }

    public List<User> createUser(User user) {
        entityManagerFactory = Persistence.createEntityManagerFactory("bicho");
        manager = entityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = manager.getTransaction();
        entityTransaction.begin();
        manager.persist(user);
        entityTransaction.commit();
        List<User> listaUsuarios =  manager.createQuery("SELECT u FROM User u").getResultList();
        return listaUsuarios;
    }

    public List<User> findById(Long id) {
        entityManagerFactory = Persistence.createEntityManagerFactory("bicho");
        manager = entityManagerFactory.createEntityManager();
        Query query = manager.createQuery("SELECT u FROM User u WHERE u.id = :id").setParameter("id", id);
        return query.getResultList();
    }
}
